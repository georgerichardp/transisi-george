# transisi_george

## Tentang Project
aplikasi ini dibangun dengan menggunakan [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) dan [SOLID principle](https://en.wikipedia.org/wiki/SOLID)
sehingga gampang dirubah dan dikembangkan

## directory
- core: merupakan kerangka project. ideal semua coding di sini tidak memeliki ketergantungan dengan package lain hanya pure dart.
  sehingga dapat di plug and play pada project yang lain jika memilik kerangka project yang sama.
- infrastructure: berisi model untuk parsing entity dan repository yang di implement
- lang: berisi service untuk multi language pada aplikasi
- modules: tedapat view, controller dan binding
- shared: terdapat class-class yang dapat digunakan untuk umum
- theme: untuk style aplikasi
- routers: untuk routes aplikasi

## failure
- ResponseError: di throw ketika terjadi error pada response
- RequestError: di throw pada handler validator

## binding
- medanftarkan dependency injection

## state management
- yang digunakan: [get](https://pub.dev/packages/get)
- state management yang lain juga saya sudah terbiasa menggunakan seperti: bloc, cubit, provider

## link apk final
- https://drive.google.com/file/d/1SSbFbwmC9HP7laYoZnVIdxRC0nM2djCB/view?usp=sharing

