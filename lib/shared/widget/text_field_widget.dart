import 'package:flutter/material.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:get/get.dart';

class TextFieldWidget extends StatelessWidget {
  final String labelText;
  final String? hintText;
  final TextInputType? keyboardType;
  final bool obscureText;
  final Widget? suffixIcon;
  final TextEditingController? controller;
  final String? Function(String? value)? validator;
  final  TextInputAction? textInputAction;
  const TextFieldWidget({
    Key? key,
    required this.labelText,
    this.hintText,
    this.keyboardType,
    this.suffixIcon,
    this.textInputAction,
    this.obscureText=false,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        hintText: hintText,
        labelText: labelText,
        suffixIcon: suffixIcon,
        hintStyle: const TextStyle(fontStyle: FontStyle.italic)
      ),
      keyboardType: keyboardType,
      validator: validator,
      obscureText:obscureText ,
      textInputAction:textInputAction ,
    );
  }
}
