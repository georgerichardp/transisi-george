import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transisi_george/shared/common/common_widget.dart';

class TextTileWidget extends StatelessWidget {
  final String title, subTitle;
  const TextTileWidget({
    Key? key,
    this.title = "",
    this.subTitle = "",
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(title,style: context.textTheme.headline6,),
        Text(subTitle,style: context.textTheme.headline5,),
        CommonWidget.rowHeight()
      ],
    );
  }
}
