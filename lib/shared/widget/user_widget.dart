import 'package:flutter/material.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:get/get.dart';

class UserWidget extends StatelessWidget {
  final String imageUrl;
  final String name;
  final String email;
  final VoidCallback onTap;

  const UserWidget({
    Key? key,
    this.imageUrl = "",
    this.name = "-",
    this.email = "-",
    required this.onTap
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Image.network(
                  imageUrl,
                  width: 80,
                ),
              ),
              CommonWidget.rowWidth(),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    name,
                    style: context.textTheme.headline5,
                  ),
                  Text(email),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
