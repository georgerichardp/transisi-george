import 'package:get/get.dart';
class CommonString{
  static String translate(String textConcat){
    var data = textConcat.split("#");
    var text = "";
    for (var element in data) {
      var index = data.indexOf(element);
      text+=index==0?element.tr:" "+element.tr.toLowerCase();
    }
    return text;
  }
}