class ResponseError extends Error {
  final String message;
  final String title;
  final int code;

  ResponseError({
    required this.message,
    this.title = "",
    this.code = 0,
  });
}
