import 'package:flutter/material.dart';
import 'package:transisi_george/routes/app_pages.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:get/get.dart';
import 'package:transisi_george/shared/widget/user_widget.dart';

import 'user_controller.dart';

class UserPage extends GetView<HomeController> {
  const UserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text("employee".tr),
        actions: [
          InkWell(
            onTap: () {
              CommonWidget.showAlert(
                "logout_alert".tr,
                pressedOk: () {
                  controller.logOut();
                },
              );
            },
            child: const Icon(Icons.logout),
          ),
          CommonWidget.rowWidth()
        ],
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          controller.getUsers();
        },
        child: Obx(() {
          if (controller.listUser.value != null) {
            var data = controller.listUser.value!;
            return ListView(
              controller: controller.scrollController,
              children: [
                ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) =>
                      CommonWidget.rowHeight(8),
                  itemCount: data.length,
                  itemBuilder: (context, index) {
                    var item = data[index];
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: UserWidget(
                        email: item.email,
                        imageUrl: item.avatar,
                        name: item.firstName + " " + item.lastName,
                        onTap: () {
                          goToUserDetail(arguments: item.id);
                        },
                      ),
                    );
                  },
                ),
                controller.page <= controller.totalPage
                    ? const Center(child: CircularProgressIndicator())
                    : const SizedBox(),
                CommonWidget.rowHeight(80)
              ],
            );
          }
          return const Center(child: CircularProgressIndicator());
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          goToUserCreate();
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
