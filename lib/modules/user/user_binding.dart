import 'package:transisi_george/core/application/feature/user/query/all/user_all_handler.dart';
import 'package:get/get.dart';

import 'user_controller.dart';

class UserBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => HomeController(
        userAllHandler: UserAllHandler(userRepository: Get.find())
      ),
    );
  }
}
