import 'package:get/get.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_handler.dart';
import 'package:transisi_george/modules/user/create/user_create_controller.dart';

class UserCreateBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => UserCreateController(
      userCreateHandler: UserCreateHandler(userRepository: Get.find())
    ));
  }

}