import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transisi_george/modules/user/create/user_create_controller.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/widget/text_field_widget.dart';

class UserCreatePage extends GetView<UserCreateController> {
  const UserCreatePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("create".tr+" "+"employee".tr),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: controller.keyForm,
          child: Column(
            children: [
              CommonWidget.rowHeight(40),
              TextFieldWidget(
                labelText: "name".tr,
                controller: controller.controllerName,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "insert".tr + " " + "name".tr;
                  }
                },
              ),
              TextFieldWidget(
                labelText: "job".tr,
                controller: controller.controllerJob,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "insert".tr + " " + "job".tr;
                  }
                },
              ),
              CommonWidget.rowHeight(),
              ElevatedButton(
                  onPressed: () {
                    controller.createSubmit();
                  },
                  child: const Text("Submit"))
            ],
          ),
        ),
      ),
    );
  }
}
