import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_command.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_handler.dart';
import 'package:transisi_george/shared/common/common_string.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/failure/request_error.dart';
import 'package:transisi_george/shared/failure/response_error.dart';

class UserCreateController extends GetxController {
  UserCreateHandler userCreateHandler;

  UserCreateController({required this.userCreateHandler});
  TextEditingController controllerName = TextEditingController();
  TextEditingController controllerJob = TextEditingController();
  GlobalKey<FormState> keyForm = GlobalKey();
  Future<void> createSubmit() async {
    UserCreateCommand request = UserCreateCommand(
      name: controllerName.text,
      job: controllerJob.text,
    );
    keyForm.currentState!.validate();
    try {
      var res = await userCreateHandler.handleAsync(request);
      if (res != null) {
        await CommonWidget.showInfo(res.message,isSuccess: res.success);
        if (res.success) {
          Get.back();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    } on RequestError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    }
  }
}
