import 'package:flutter/material.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_handler.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_query.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_vm.dart';
import 'package:transisi_george/routes/app_pages.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/constants/constant_storage.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:transisi_george/shared/failure/request_error.dart';
import 'package:transisi_george/shared/failure/response_error.dart';

class HomeController extends GetxController {
  UserAllHandler userAllHandler;

  HomeController({
    required this.userAllHandler,
  });
  var pref = Get.find<SharedPreferences>();
  var listUser = Rxn<List<UserAllVM>>();
  int page = 1;
  int totalPage = 1;
  ScrollController scrollController = ScrollController();
  @override
  void onInit() {
    super.onInit();
    getUsers();
    scrollController.addListener(onScroll);
  }

  void logOut() {
    pref.remove(ConstantStorage.token);
    goToInitial();
  }
  Future<void> getUsers() async {
    UserAllQuery request = UserAllQuery(page: 1.toString());
    try {
      var res = await userAllHandler.handleAsync(request);
      if (res != null) {
        if (res.success) {
          page = res.page+1;
          totalPage = res.totalPage;
          listUser.value = res.data;
          listUser.refresh();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showSnackBar(e.message);
    } on RequestError catch (e) {
      CommonWidget.showSnackBar(e.message);
    }
  }
  Future<void> usersLoadMore() async {
    UserAllQuery request = UserAllQuery(page: page.toString());
    try {
      if (page<= totalPage) {
        var res = await userAllHandler.handleAsync(request);
        if (res != null) {
          if (res.success) {
            page = res.page+1;
            for (var element in res.data!) {
              listUser.value!.add(element);
            }
            listUser.refresh();
          }
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showSnackBar(e.message);
    } on RequestError catch (e) {
      CommonWidget.showSnackBar(e.message);
    }
  }

  void onScroll() {
    double maxScroll = scrollController.position.maxScrollExtent;
    double currentScroll = scrollController.position.pixels;
    if (currentScroll == maxScroll) {
      usersLoadMore();
    }
  }
}
