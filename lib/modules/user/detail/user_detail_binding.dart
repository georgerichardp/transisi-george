import 'package:get/get.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_handler.dart';

import 'user_detail_controller.dart';

class UserDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => UserDetailController(
        userDetailHandler: UserDetailHandler(userRepository: Get.find())
      ),
    );
  }
}
