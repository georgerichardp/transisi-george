import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/widget/text_tile_widget.dart';
import 'user_detail_controller.dart';

class UserDetailPage extends GetView<UserDetailController> {
  const UserDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("detail".tr+" "+"employee".tr),
      ),
      body: Obx(() {
        if (controller.userDetail.value != null) {
          var item = controller.userDetail.value!;
          return Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children:  [
                CommonWidget.rowHeight(40),
                Image.network(item.avatar),
                CommonWidget.rowHeight(),
                TextTileWidget(
                  title: "first_name".tr,
                  subTitle: item.firstName,
                ),
                TextTileWidget(
                  title: "last_name".tr,
                  subTitle: item.lastName,
                ),
                TextTileWidget(
                  title: "email".tr,
                  subTitle: item.email,
                ),
              ],
            ),
          );
        }
        return const Center(child: CircularProgressIndicator());
      }),
    );
  }
}
