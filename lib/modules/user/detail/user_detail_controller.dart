import 'package:get/get.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_handler.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_query.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_vm.dart';
import 'package:transisi_george/shared/common/common_string.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/failure/request_error.dart';
import 'package:transisi_george/shared/failure/response_error.dart';

class UserDetailController extends GetxController {
  UserDetailHandler userDetailHandler;
  var userId = Get.arguments;
  var userDetail = Rxn<UserDetailVM>();

  UserDetailController({required this.userDetailHandler});
  Future<void> getDetail() async {
    UserDetailQuery request = UserDetailQuery(userId: userId.toString());
    try {
      var res = await userDetailHandler.handleAsync(request);
      if (res != null) {
        if (res.success) {
          userDetail.value = res.data;
          userDetail.refresh();
        }else{
          await CommonWidget.showInfo(res.message);
          Get.back();
        }
      }
    } on ResponseError catch (e) {
      await CommonWidget.showInfo(CommonString.translate(e.message));
      Get.back();
    } on RequestError catch (e) {
      await CommonWidget.showInfo(CommonString.translate(e.message));
      Get.back();
    }
  }
  @override
  void onInit() {
    super.onInit();
    getDetail();
  }
}
