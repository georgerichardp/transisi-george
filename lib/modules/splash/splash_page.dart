import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:transisi_george/modules/user/user_controller.dart';

class SplashPage extends GetView<HomeController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Transisi",
            style: context.textTheme.headline3!.copyWith(color: context.theme.primaryColor),
          ),
          const CircularProgressIndicator()
        ],
      ),
    );
  }
}
