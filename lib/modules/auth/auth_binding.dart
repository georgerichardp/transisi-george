import 'package:transisi_george/core/application/feature/auth/command/login/login_handler.dart';
import 'package:transisi_george/core/application/feature/auth/command/register/register_handler.dart';
import 'package:transisi_george/modules/auth/auth_controller.dart';
import 'package:get/get.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => AuthController(
        loginHandler: LoginHandler(repository: Get.find()),
        registerHandler: RegisterHandler(authRepository: Get.find()),
      ),
    );
  }
}
