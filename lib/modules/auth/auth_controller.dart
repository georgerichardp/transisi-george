import 'package:flutter/material.dart';
import 'package:transisi_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:transisi_george/core/application/feature/auth/command/login/login_handler.dart';
import 'package:transisi_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:transisi_george/core/application/feature/auth/command/register/register_handler.dart';
import 'package:transisi_george/routes/app_pages.dart';
import 'package:transisi_george/shared/common/common_string.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/constants/constant_storage.dart';
import 'package:transisi_george/shared/failure/request_error.dart';
import 'package:transisi_george/shared/failure/response_error.dart';
import 'package:transisi_george/shared/model/simple_model.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthController extends GetxController {
  final LoginHandler loginHandler;
  final RegisterHandler registerHandler;

  AuthController({
    required this.loginHandler,
    required this.registerHandler,
  });

  GlobalKey keyLanguage = GlobalKey();
  GlobalKey<FormState> keyFormLogin = GlobalKey();
  GlobalKey<FormState> keyFormRegister = GlobalKey();
  List<SimpleModel<String>> listLanguage = [
    SimpleModel(
      title: "English",
      data: "assets/icons/united-states.png",
    ),
    SimpleModel(
      title: "Indonesia",
      data: "assets/icons/indonesia.png",
    )
  ];
  var valueLanguage = Rxn<SimpleModel>();
  var pref = Get.find<SharedPreferences>();
  //Text editing controller login
  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPass = TextEditingController();
  //Text editing controller register
  TextEditingController controllerRegEmail = TextEditingController();
  TextEditingController controllerRegPass = TextEditingController();
  TextEditingController controllerRegConfirmPass = TextEditingController();
  var obscurePass = Rxn<bool>(true);
  var obscurePassReq = Rxn<bool>(true);
  @override
  void onInit() {
    super.onInit();
    setLanguage();
  }

  void updateLanguage(String language) {
    Locale locale = const Locale("id", "ID");
    if (language == "English") {
      locale = const Locale("en", "US");
    }
    pref.setString(ConstantStorage.language, locale.languageCode);
    Get.updateLocale(locale);
    setLanguage();
  }

  void setLanguage() {
    if (pref.getString(ConstantStorage.language) == "id") {
      valueLanguage(listLanguage[1]);
    } else {
      valueLanguage(listLanguage[0]);
    }
  }

  Future<void> submitLogin() async {
    keyFormLogin.currentState!.validate();
    LoginCommand command = LoginCommand(
      email: controllerEmail.text,
      password: controllerPass.text,
    );
    try {
      var res = await loginHandler.handleAsync(command);
      if (res != null) {
        await CommonWidget.showInfo(CommonString.translate(res.message),
            isSuccess: res.success);
        if (res.success) {
          pref.setString(ConstantStorage.token, res.data!.token);
          goToInitial();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    } on RequestError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    }
  }

  Future<void> submitRegister() async {
    keyFormRegister.currentState!.validate();
    RegisterCommand command = RegisterCommand(
      userEmail: controllerRegEmail.text,
      password: controllerRegPass.text,
      confirmPassword: controllerRegConfirmPass.text,
    );
    try {
      var res = await registerHandler.handleAsync(command);
      if (res != null) {
        await CommonWidget.showInfo(CommonString.translate(res.message),
            isSuccess: res.success);
        if (res.success) {
          disposeRegister();
        }
      }
    } on ResponseError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    } on RequestError catch (e) {
      CommonWidget.showInfo(CommonString.translate(e.message));
    }
  }

  void disposeRegister() {
    Get.back();
    controllerRegEmail.clear();
    controllerRegPass.clear();
    controllerRegConfirmPass.clear();
  }
}
