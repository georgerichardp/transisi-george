import 'package:flutter/material.dart';
import 'package:transisi_george/modules/auth/auth_controller.dart';
import 'package:transisi_george/modules/auth/register_page.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/widget/text_field_widget.dart';
import 'package:get/get.dart';

class LoginPage extends GetView<AuthController> {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: Padding(
                padding:  EdgeInsets.only(left: 16,right: 16,bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Form(
                  key: controller.keyFormLogin,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CommonWidget.rowHeight(250),
                      Text(
                        "login".tr,
                        style: context.textTheme.headline4,
                      ),
                      Text(
                        "login_info".tr,
                        style: context.textTheme.subtitle1,
                      ),
                      TextFieldWidget(
                        labelText: "email".tr,
                        // hintText: "user_id".tr,
                        textInputAction: TextInputAction.next,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "insert".tr + " " + "email".tr;
                          }
                        },
                        controller: controller.controllerEmail,
                      ),
                      Obx(() {
                        return TextFieldWidget(
                          labelText: "password".tr,
                          // hintText: "password".tr,
                          controller: controller.controllerPass,
                          obscureText: controller.obscurePass.value!,
                          textInputAction: TextInputAction.done,
                          suffixIcon: InkWell(
                            onTap: () {
                              controller
                                  .obscurePass(!controller.obscurePass.value!);
                            },
                            child: Icon(controller.obscurePass.value!
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "insert".tr + " " + "password".tr;
                            }
                          },
                        );
                      }),
                      CommonWidget.rowHeight(),
                      CommonWidget.rowHeight(),
                      Align(
                        alignment: Alignment.topRight,
                        child: ElevatedButton(
                          onPressed: () {
                            controller.submitLogin();
                          },
                          child: Text("login".tr.toUpperCase()),
                        ),
                      ),
                      CommonWidget.rowHeight(),
                      CommonWidget.rowHeight(),
                    ],
                  ),
                ),
              ),
            ),
            Obx(() {
              return Align(
                alignment: Alignment.topRight,
                child: InkWell(
                  onTap: () {
                    CommonWidget.showDialog(
                      builder: _buildListLanguage(context),
                    );
                  },
                  child: Padding(
                    key: controller.keyLanguage,
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("language".tr,style: context.textTheme.headline5,),
                        CommonWidget.rowWidth(),
                        Image.asset(
                          controller.valueLanguage.value!.data!,
                          width: 25,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }),
            Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("don't_have_account".tr),
                  InkWell(
                    onTap: () {
                      Get.to(() => RegisterPage(), arguments: controller);
                    },
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 8),
                      child: Text(
                        "sign_up".tr,
                        style: context.textTheme.headline5,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildListLanguage(BuildContext context) {
    var data = controller.listLanguage;
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              CommonWidget.rowHeight(),
              Text(
                "choose".tr + " " + "language".tr,
                style: context.textTheme.headline6,
              ),
              ListView.separated(
                separatorBuilder: (context, index) => const Divider(),
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (context, index) {
                  var item = data[index];
                  return InkWell(
                    onTap: () {
                      controller.updateLanguage(item.title);
                      Get.back();
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Image.asset(
                            item.data ?? "",
                            width: 35,
                          ),
                          CommonWidget.rowWidth(),
                          Text(item.title),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: data.length,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
