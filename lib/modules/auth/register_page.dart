import 'package:flutter/material.dart';
import 'package:transisi_george/modules/auth/auth_controller.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/widget/text_field_widget.dart';
import 'package:get/get.dart';

class RegisterPage extends StatelessWidget {
  final AuthController controller = Get.arguments;
  RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("sign_up".tr),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Obx(() {
              return Form(
                key: controller.keyFormRegister,
                child: Column(
                  children: [
                    TextFieldWidget(
                      labelText: "email".tr,
                      controller: controller.controllerRegEmail,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"email".tr;
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "password".tr,
                      controller: controller.controllerRegPass,
                      obscureText: controller.obscurePassReq.value!,
                      textInputAction: TextInputAction.next,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"password".tr.toLowerCase();
                        }
                      },
                    ),
                    TextFieldWidget(
                      labelText: "confirm".tr + " " + "password".tr,
                      controller: controller.controllerRegConfirmPass,
                      obscureText: controller.obscurePassReq.value!,
                      textInputAction: TextInputAction.done,
                      suffixIcon: InkWell(
                        onTap: () {
                          controller.obscurePassReq(
                              !controller.obscurePassReq.value!);
                        },
                        child: Icon(controller.obscurePassReq.value!
                            ? Icons.visibility_off
                            : Icons.visibility),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "insert".tr+" "+"confirm".tr+" "+"password".tr.toLowerCase();
                        }
                        if (value != controller.controllerRegPass.text) {
                          return "pass_not_match".tr;
                        }
                      },
                    ),
                    CommonWidget.rowHeight(),
                    CommonWidget.rowHeight(),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: ElevatedButton(
                        onPressed: () {
                          controller.submitRegister();
                        },
                        child: Text("sign_up".tr.toUpperCase()),
                      ),
                    )
                  ],
                ),
              );
            }
          ),
        ),
      ),
    );
  }
}
