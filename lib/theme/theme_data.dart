import 'package:flutter/material.dart';

class ThemeConfig {
  static ThemeData createTheme({
    required Color primaryColor,
    required Color errorColor,
    required Color disableColor,
    required Color accentColor,
  }) {
    return ThemeData(
      brightness: Brightness.light,
      scaffoldBackgroundColor: Colors.white,
      primaryColor: primaryColor,
      errorColor: errorColor,
      disabledColor: disableColor,
      cardTheme: CardTheme(
          margin: const EdgeInsets.all(8),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
          elevation: 5,
          shadowColor: disableColor.withOpacity(0.25)),
      indicatorColor: accentColor,
      outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 16),
      )),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          elevation: 0,
          padding: const EdgeInsets.symmetric(horizontal: 42, vertical: 16),
        ).copyWith(
          backgroundColor: MaterialStateProperty.resolveWith((states) =>
              states.contains(MaterialState.disabled)
                  ? disableColor.withOpacity(0.6)
                  : primaryColor),
        ),
      ),
      bottomNavigationBarTheme: BottomNavigationBarThemeData(
        selectedItemColor: accentColor,
      ),
      bottomSheetTheme: const BottomSheetThemeData(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
        ),
      ),
      textTheme: TextTheme(
          headline6: TextStyle(
              color: accentColor, fontWeight: FontWeight.bold, fontSize: 14),
          headline5: TextStyle(
              fontSize: 14, fontWeight: FontWeight.bold, color: primaryColor),
          headline4: const TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.black)),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: primaryColor,
        foregroundColor: Colors.white,
      ),
      colorScheme: ColorScheme.light(primary: primaryColor)
          .copyWith(secondary: accentColor),
    );
  }

  static ThemeData get lightTheme => createTheme(
        accentColor: const Color(0xFF191919),
        errorColor: const Color(0xFFBF0101),
        disableColor: const Color(0xFF5A5A5A),
        primaryColor: const Color(0xFF3F51B5),
      );
}
