class ResponseEntity<Datum>{
  String message;
  bool success;
  Datum? data;

  ResponseEntity({required this.message, required this.success, this.data});

  factory ResponseEntity.success(Datum? datum){
    return ResponseEntity(message: "Success", success: true,data: datum);
  }
}