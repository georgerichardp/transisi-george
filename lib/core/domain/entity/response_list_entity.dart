class ResponseListEntity<Datum> {
  String message;
  bool success;
  int page;
  int totalPage;
  List<Datum>? data;

  ResponseListEntity({
    required this.message,
    required this.success,
    required this.totalPage,
    required this.page,
    this.data,
  });

  factory ResponseListEntity.success({
    required int totalPage,
    required int page,
    List<Datum>? data,
  }) {
    return ResponseListEntity(
      message: "Success",
      success: true,
      totalPage: totalPage,
      page: page,
      data: data ?? [],
    );
  }
}
