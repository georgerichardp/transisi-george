import 'package:transisi_george/core/application/feature/user/command/create/user_create_command.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_query.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_query.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';
import 'package:transisi_george/core/domain/entity/response_list_entity.dart';
import 'package:transisi_george/core/domain/entity/user_entity.dart';

abstract class UserRepository{
  Future<ResponseEntity<UserEntity>?> getDetail(UserDetailQuery userDetailQuery);
  Future<ResponseListEntity<UserEntity>?> getAll(UserAllQuery userAllQuery);
  Future<ResponseEntity?> create(UserCreateCommand userCreateCommand);
}