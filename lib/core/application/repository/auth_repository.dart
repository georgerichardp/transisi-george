import 'package:transisi_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:transisi_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:transisi_george/core/domain/entity/auth_entity.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';

abstract class AuthRepository{
  Future<ResponseEntity<AuthEntity>?> login(LoginCommand command);
  Future<ResponseEntity?> register(RegisterCommand command);
}