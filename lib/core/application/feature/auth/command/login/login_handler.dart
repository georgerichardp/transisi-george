import 'package:transisi_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:transisi_george/core/application/feature/auth/command/login/login_validator.dart';
import 'package:transisi_george/core/application/feature/handler.dart';
import 'package:transisi_george/core/application/repository/auth_repository.dart';
import 'package:transisi_george/core/domain/entity/auth_entity.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';

class LoginHandler extends Handler<ResponseEntity<AuthEntity>?, LoginCommand> {
  final AuthRepository repository;

  LoginHandler({required this.repository});
  @override
  Future<ResponseEntity<AuthEntity>?> handleAsync(LoginCommand request) {
    LoginValidator().validate(request);
    return repository.login(request);
  }
}
