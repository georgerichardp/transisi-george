import 'package:transisi_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:transisi_george/core/application/feature/command_validator.dart';
import 'package:transisi_george/shared/failure/request_error.dart';

class LoginValidator extends CommandValidator<LoginCommand>{
  @override
  void validate(LoginCommand element) {
    if (element.email.isEmpty) {
      throw RequestError(message: "please_insert#email");
    }
    if (element.password.isEmpty) {
      throw RequestError(message: "please_insert#password");
    }
  }

}