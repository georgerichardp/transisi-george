class LoginCommand{
  final String email;
  final String password;

  LoginCommand({required this.email, required this.password});

  Map<String, dynamic> toJson() => {
    "email": email,
    "password": password,
  };

}