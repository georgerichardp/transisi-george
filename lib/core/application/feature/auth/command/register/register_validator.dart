import 'package:transisi_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:transisi_george/core/application/feature/command_validator.dart';
import 'package:transisi_george/shared/failure/request_error.dart';

class RegisterValidator extends CommandValidator<RegisterCommand>{
  @override
  void validate(RegisterCommand element) {
    if (element.userEmail.isEmpty) {
      throw RequestError(message: "please_insert#name");
    }
    if (element.password.isEmpty) {
      throw RequestError(message: "please_insert#password");
    }
    if (element.confirmPassword.isEmpty) {
      throw RequestError(message: "please_insert#confirm#password");
    }
    if (element.password != element.confirmPassword) {
      throw RequestError(message: "pass_not_match");
    }
  }

}