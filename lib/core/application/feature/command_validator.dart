abstract class CommandValidator<E>{
  void validate(E element);
}