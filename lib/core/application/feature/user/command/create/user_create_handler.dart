import 'package:transisi_george/core/application/feature/handler.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_command.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_validator.dart';
import 'package:transisi_george/core/application/repository/user_repository.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';

class UserCreateHandler extends Handler<ResponseEntity,UserCreateCommand>{
  UserRepository userRepository;

  UserCreateHandler({required this.userRepository});

  @override
  Future<ResponseEntity?> handleAsync(UserCreateCommand request) {
    UserCreateValidator().validate(request);
    return  userRepository.create(request);
  }

}