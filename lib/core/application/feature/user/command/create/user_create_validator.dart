import 'package:transisi_george/core/application/feature/command_validator.dart';
import 'package:transisi_george/core/application/feature/user/command/create/user_create_command.dart';
import 'package:transisi_george/shared/failure/request_error.dart';

class UserCreateValidator extends CommandValidator<UserCreateCommand>{
  @override
  void validate(UserCreateCommand element) {
    if (element.name.isEmpty) {
      throw RequestError(message: "please_insert#name");
    }
    if (element.name.isEmpty) {
      throw RequestError(message: "please_insert#job");
    }
  }

}