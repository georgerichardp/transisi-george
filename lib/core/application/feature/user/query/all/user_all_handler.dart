import 'package:transisi_george/core/application/feature/handler.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_query.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_vm.dart';
import 'package:transisi_george/core/application/repository/user_repository.dart';
import 'package:transisi_george/core/domain/entity/response_list_entity.dart';

class UserAllHandler
    extends Handler<ResponseListEntity<UserAllVM>, UserAllQuery> {
  UserRepository userRepository;

  UserAllHandler({
    required this.userRepository,
  });

  @override
  Future<ResponseListEntity<UserAllVM>?> handleAsync(
      UserAllQuery request) async {
    var res = await userRepository.getAll(request);
    if (res != null) {
      return ResponseListEntity(
        message: res.message,
        success: res.success,
        totalPage: res.totalPage,
        page: res.page,
        data: res.data == null
            ? []
            : res.data!
                .map((e) => UserAllVM(
                      id: e.id,
                      email: e.email,
                      firstName: e.firstName,
                      lastName: e.lastName,
                      avatar: e.avatar,
                    ))
                .toList(),
      );
    }
  }
}
