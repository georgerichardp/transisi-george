class UserAllQuery {
  UserAllQuery({
    this.page,
  });

  String? page;

  Map<String, dynamic> toJson() => {
    "page":page,
    "per_page": 6.toString(),
  };
}