import 'package:transisi_george/core/application/feature/handler.dart';
import 'package:transisi_george/core/application/repository/user_repository.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';

import 'user_detail_query.dart';
import 'user_detail_vm.dart';

class UserDetailHandler
    extends Handler<ResponseEntity<UserDetailVM>, UserDetailQuery> {
  UserRepository userRepository;

  UserDetailHandler({required this.userRepository});

  @override
  Future<ResponseEntity<UserDetailVM>?> handleAsync(
      UserDetailQuery request) async {
    var res = await userRepository.getDetail(request);
    if (res != null) {
      return ResponseEntity(
        message: res.message,
        success: res.success,
        data: res.data == null
            ? null
            : UserDetailVM(
                id: res.data!.id,
                email: res.data!.email,
                firstName: res.data!.firstName,
                lastName: res.data!.lastName,
                avatar: res.data!.avatar,
              ),
      );
    }
  }
}
