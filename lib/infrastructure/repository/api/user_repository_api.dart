import 'package:transisi_george/core/application/feature/user/command/create/user_create_command.dart';
import 'package:transisi_george/core/application/feature/user/query/all/user_all_query.dart';
import 'package:transisi_george/core/application/feature/user/query/detail/user_detail_query.dart';
import 'package:transisi_george/core/application/repository/user_repository.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';
import 'package:transisi_george/core/domain/entity/response_list_entity.dart';
import 'package:transisi_george/core/domain/entity/user_entity.dart';
import 'package:transisi_george/infrastructure/model/user_model.dart';
import 'package:transisi_george/infrastructure/repository/api/core/base_provider.dart';

class UserRepositoryApi extends BaseProvider implements UserRepository {
  var url = "users";
  @override
  Future<ResponseEntity?> create(UserCreateCommand userCreateCommand) async {
    var body = userCreateCommand.toJson();
    printBody(body);
    var api = await post(url, body);
    if (checkApi(api)) {
      return ResponseEntity.success(
        api.body["data"],
      );
    }
  }

  @override
  Future<ResponseListEntity<UserEntity>?> getAll(
      UserAllQuery userAllQuery) async {
    var api = await get(url, query: userAllQuery.toJson());
    if (checkApi(api)) {
      return ResponseListEntity.success(
        totalPage: api.body["total_pages"],
        page: api.body["page"],
        data: api.body["data"] == null
            ? []
            : UserModel.parseEntries(api.body["data"]),
      );
    }
  }

  @override
  Future<ResponseEntity<UserEntity>?> getDetail(
      UserDetailQuery userDetailQuery) async {
    var api = await get(url + "/${userDetailQuery.userId}");
    if (checkApi(api)) {
      return ResponseEntity.success(
        UserModel.fromJson(api.body["data"]),
      );
    }
  }
}
