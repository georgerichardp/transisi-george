import 'package:transisi_george/core/application/feature/auth/command/login/login_command.dart';
import 'package:transisi_george/core/application/feature/auth/command/register/register_command.dart';
import 'package:transisi_george/core/application/repository/auth_repository.dart';
import 'package:transisi_george/core/domain/entity/auth_entity.dart';
import 'package:transisi_george/core/domain/entity/response_entity.dart';
import 'package:transisi_george/infrastructure/model/auth_model.dart';
import 'package:transisi_george/infrastructure/repository/api/core/base_provider.dart';

class AuthRepositoryApi extends BaseProvider implements AuthRepository {
  @override
  Future<ResponseEntity<AuthEntity>?> login(LoginCommand command) async {
    var body = command.toJson();
    printBody(body);
    var api = await post("login", body);
    if (checkApi(api)) {
      return ResponseEntity.success(
        AuthModel.fromJson(api.body),
      );
    }
  }

  @override
  Future<ResponseEntity?> register(RegisterCommand command) async {
    var body = command.toJson();
    printBody(body);
    var api = await post("register", body);
    if (checkApi(api)) {
      return ResponseEntity.success(api.body);
    }
  }
}
