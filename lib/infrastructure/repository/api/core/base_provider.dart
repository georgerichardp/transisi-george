import 'dart:convert';
import 'package:get/get.dart';
import 'package:transisi_george/shared/common/common.dart';
import 'api_constants.dart';
import 'interceptors/auth_interceptor.dart';
import 'interceptors/request_interceptor.dart';
import 'interceptors/response_interceptor.dart';

class BaseProvider extends GetConnect {
  bool checkApi(Response api) {
    if (api.statusCode == 200) {
      return true;
    }
    if (api.statusCode == 201) {
      return true;
    }
    return false;
  }
  void printBody(body){
    Common.printWrapped("body => "+jsonEncode(body));
  }

  @override
  void onInit() {
    httpClient.timeout = const Duration(minutes: 5);
    httpClient.baseUrl = ApiConstants.baseUrl;
    httpClient.addAuthenticator(authInterceptor);
    httpClient.addRequestModifier(requestInterceptor);
    httpClient.addResponseModifier(responseInterceptor);
  }
}
