import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:transisi_george/shared/common/common.dart';
import 'package:transisi_george/shared/common/common_widget.dart';
import 'package:transisi_george/shared/failure/response_error.dart';

FutureOr<dynamic> responseInterceptor(
    Request request, Response response) async {
  if (request.method == "post" ||
      request.method == "put" ||
      request.method == "delete") {
    CommonWidget.dismissLoading();
  }

  log('===>>');
  log(request.method);
  log('url: ${request.url}');
  log('headers: ${request.headers}');
  log('<<===');
  log('Status Code: ${response.statusCode}');
  log('response: ');
  Common.printWrapped(jsonEncode(response.body));
  log('\n');

  if (response.statusCode == 200) {
    return response;
  } else if (response.statusCode == 201) {
    return response;
  }else{
    handleErrorStatus(response);
    return;
  }
}

void handleErrorStatus(Response response) {
  if (response.statusCode != 200) {
    throw ResponseError(message: response.body['error'] ?? "Something wrong");
  }
  if (response.statusCode != 201) {
    throw ResponseError(message: response.body['error'] ?? "Something wrong");
  }
}
