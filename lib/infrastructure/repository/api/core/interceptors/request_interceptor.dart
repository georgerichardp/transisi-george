import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:transisi_george/shared/common/common_widget.dart';

FutureOr<Request>   requestInterceptor(Request request) async {
  if (request.method=="post" || request.method=="put"|| request.method=="delete") {
    if (FocusManager.instance.primaryFocus!= null) {
      FocusManager.instance.primaryFocus!.unfocus();
    }
    CommonWidget.showLoading();
  }
  return request;
}
