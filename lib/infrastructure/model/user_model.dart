import 'package:transisi_george/core/domain/entity/user_entity.dart';

class UserModel extends UserEntity {
  UserModel.fromJson(Map<String, dynamic> json)
      : super(
          id: json["id"],
          email: json["email"],
          firstName: json["first_name"],
          lastName: json["last_name"],
          avatar: json["avatar"],
        );
  static List<UserEntity> parseEntries(List<dynamic> entries){
    return entries.map((e) => UserModel.fromJson(e)).toList();
  }
}
