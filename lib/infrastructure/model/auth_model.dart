import 'package:transisi_george/core/domain/entity/auth_entity.dart';

class AuthModel extends AuthEntity{
  AuthModel.fromJson(Map<String,dynamic> json) : super(
    token: json["token"],
  );

}