import 'package:transisi_george/core/application/repository/auth_repository.dart';
import 'package:get/get.dart';
import 'package:transisi_george/core/application/repository/user_repository.dart';
import 'package:transisi_george/infrastructure/repository/api/auth_repository_api.dart';
import 'package:transisi_george/infrastructure/repository/api/user_repository_api.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put<AuthRepository>(AuthRepositoryApi(), permanent: true);
    Get.put<UserRepository>(UserRepositoryApi(),permanent: true);
  }
}
