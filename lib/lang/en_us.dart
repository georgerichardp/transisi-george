const Map<String, String> enUs = {
  "login": "Login",
  "login_info": "Please sign in to continue",
  "email": "Email",
  "password": "Password",
  "language": "Language",
  "language_set_info": "Do this to change language",
  "choose": "Choose",
  "insert": "Insert",
  "don't_have_account": "Don't have an account?",
  "sign_up": "Sign Up",
  "name": "Name",
  "phone_number": "Phone Number",
  "confirm": "Confirm",
  "pass_not_match": "Password does not match",
  "please_insert": "Please insert",
  "user_not_register": "User is not registered.",
  "please": "Please",
  "login_success": "Login successfully",
  "password_wrong": "Wrong password",
  "user_name": "User Name",
  "log_out": "Log out",
  "register_success": "Successfully registered account",
  "user_already": "User ID already registered",
  "welcome": "Welcome",
  "ok": "Ok",
  "cancel": "Cancel",
  "logout_alert":"Are you sure you want to log out?",
  "first_name":"First Name",
  "last_name":"Last Name",
  "job":"Job",
  "employee":"Employee",
  "create":"Create",
  "detail":"Detail"
};
