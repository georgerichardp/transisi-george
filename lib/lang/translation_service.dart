import 'package:flutter/material.dart';
import 'package:transisi_george/shared/constants/constant_storage.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'en_us.dart';
import 'id.dart';

class TranslationService extends Translations {
  static Locale? get locale => Get.deviceLocale;
  static const fallbackLocale = Locale('en', 'US');
  @override
  Map<String, Map<String, String>> get keys => {
        'en_us': enUs,
        'id': id,
      };
  static Locale language(){
    var pref = Get.find<SharedPreferences>();
    if (pref.getString(ConstantStorage.language)=="id") {
      return const Locale("id","ID");
    }
    return const Locale("en","US");
  }
}
