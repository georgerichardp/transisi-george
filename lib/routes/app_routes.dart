class AppRoutes {
  static const splash = '/';
  static const auth = '/auth';
  static const login = '/login';
  static const register = '/register';
  static const user = '/user';
  static const detail = '/detail';
  static const create = '/create';
}
