import 'package:transisi_george/modules/auth/auth_binding.dart';
import 'package:transisi_george/modules/auth/login_page.dart';
import 'package:transisi_george/modules/splash/splash_binding.dart';
import 'package:transisi_george/modules/splash/splash_page.dart';
import 'package:get/get.dart';
import 'package:transisi_george/modules/user/create/user_create_binding.dart';
import 'package:transisi_george/modules/user/create/user_create_page.dart';
import 'package:transisi_george/modules/user/detail/user_detail_binding.dart';
import 'package:transisi_george/modules/user/detail/user_detail_page.dart';
import 'package:transisi_george/modules/user/user_binding.dart';
import 'package:transisi_george/modules/user/user_page.dart';
import 'app_routes.dart';

class AppPages {
  static const initial = AppRoutes.splash;

  static final routes = [
    GetPage(
      name: AppRoutes.splash,
      page: () => const SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.auth,
      page: () => const LoginPage(),
      binding: AuthBinding(),
    ),
    GetPage(
        name: AppRoutes.user,
        page: () => const UserPage(),
        binding: UserBinding(),
        children: [
          GetPage(
            name: AppRoutes.detail,
            page: () => const UserDetailPage(),
            binding: UserDetailBinding(),
          ),
          GetPage(
            name: AppRoutes.create,
            page: () => const UserCreatePage(),
            binding: UserCreateBinding(),
          ),
        ]),
  ];
}

Future<dynamic> goToUser() async {
  return Get.offAndToNamed(AppRoutes.user);
}

Future<dynamic> goToUserCreate() async {
  return Get.toNamed(AppRoutes.user + AppRoutes.create);
}

Future<dynamic> goToUserDetail({dynamic arguments}) async {
  return Get.toNamed(
    AppRoutes.user + AppRoutes.detail,
    arguments: arguments,
  );
}

Future<dynamic> goToAuth() async {
  return Get.offAndToNamed(AppRoutes.auth);
}

Future<dynamic> goToInitial() async {
  return Get.offAndToNamed(AppRoutes.splash);
}
